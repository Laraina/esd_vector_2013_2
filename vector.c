#include "vector.h"
#include <stdio.h>
#include <stdlib.h>
 
 Vector* vector_create(int block)
 {
     Vector *v;
     
     if (block <= 0)
         return NULL;
     
     v = (Vector *) malloc(sizeof(Vector));
     
     if (v != NULL) {
         v->data = (int *) calloc(block, sizeof(int));
         
         if (v->data == NULL) {
             vector_destroy(v);
             return NULL;
         }
         
         v->used = 0;
         v->size = v->block = block;
     }
     
     return v;
 }
 
 void vector_destroy(Vector* v)
 {
     free(v->data);
     free(v);
 }
 
 static int vector_resize(Vector* v, int new_size)
 {
     int* new_data = realloc(v->data, new_size * sizeof(int));
     
     if (new_data == NULL)
         return memory_e;
     
     v->data = new_data;
     v->size = new_size;
         
     return success;
 }
 
 int vector_insert(Vector* v, int pos, int data)
 {
     int i;
     
     pos = MIN(pos, v->used);
     
     if (pos < 0)
         return param_e;
     
     if (v->used == v->size)
         if (vector_resize(v, v->size + v->block))
             return memory_e;
     
     for (i = v->used; i > pos; i--)
         v->data[i] = v->data[i-1];
     
     v->data[pos] = data;
     v->used++;
     
     return success;
 }
 
 int vector_remove(Vector* v, int pos)
 {
     if (pos < 0 || pos >= v->used)
         return param_e;
     
     v->used--;
 
     while(pos++ < v->used)
         v->data[pos-1] = v->data[pos];
     
     if (v->size - v->used >= 2 * v->block)
        vector_resize(v, v->size - v->block); 
     
     return success;
 }
 
 int vector_find(Vector* v, int data)
 {
     int pos;
     
     for (pos = 0; pos < v->used; pos++)
         if (v->data[pos] == data)
             return pos;
     
     return -1;
 }
 
 int vector_count(Vector* v, int data)
 {
     int pos, count = 0;
 
     for (pos = 0; pos < v->used; pos++)
         if (v->data[pos] == data)
             count++;
     
     return count;
 }
 
//static void selection_sort(Vector* v)
static void selection_sort(int* v, int n)
 {
 	int i, j, maior, aux;
 	for(i=1; i<n; i++){
 		maior= n-i;
 		for(j=0; j<n-i; j++){
 			if (v[j]>v[maior])
 			maior = j;
 		}
 		aux = v[n-i];
 		v[n-i] = v [maior];
 		v[maior] = aux;
 	} 
 }
 
//static void insertion_sort(Vector* v)
static void insertion_sort(int* v, int n)
 {
 	int i, j, aux;
 	for (i=1; i< n; i++){
 		aux = v[i];
 		for(j = i-1; j>=0 && v[j]>aux; j--)
 		v [j+1] = v[j];
 		v[j+1] = aux;
 	}
 }
 
 static void mescla(int* v, int p, int q, int r)
 {	
 		int i, j, k, *w;
	 	w = malloc ((r-p) * sizeof (int));
	 	i=p; j=q; k=0;
	 	while (i<q && j<r)
	 	if(v[i]<=v[j])
	 	w[k++]= v[i++];
	 	else	
	 	w[k++]= v[j++];
	 	while (i<q)
	 	w[k++] = v[i++];
		 while(j<r)
		 w[k++] = v[j++];
		 while (k--)
		 v[p+k] = w[k];
		 free(w);
}

//static void merge_sort(Vector* v)
static void merge_sort(int* v, int p, int r)
 {
 	if (p < r-1)
	 {
	 int q = (p+r)/2;
	 merge_sort(v, p, q);
	 merge_sort(v, q, r);
	 mescla(v, p, q, r);
	 } 	
 }
 
//static void quick_sort(Vector* v)
static void quick_sort(int* v, int esq, int dir)
 {	
 	if(dir > esq)
	 {
		int p = quebra(v,esq,dir);
 		quick_sort(v, esq, p-1);
 		quick_sort(v, p+1, dir);
	 }
}

int quebra (int* v, int e, int d){
 	int p, i, j, t;
 	p=v[d]; 
	j=e;
 	for (i=e; i<d; i++)
 	if(v[i]<=p){
 		t = v[j], v[j]= v[i], v[i]=t;
 		j++;
 	}
 	v[d] = v[j];
 	v[j] = p;
 	return j;
 }
 
 void vector_sort(Vector* v, int sorting){
     switch (sorting) {
     	
     	case selection:
  //          selection_sort(v);
            selection_sort(v->data, v->used);
         break;
         
         case insertion:
    //        insertion_sort(v);
            insertion_sort(v->data, v->used);
         break;
         
         case merge:
      //      merge_sort(v);
            merge_sort(v->data, 0, v->used);
         break;
         
         case quick:
        //    quick_sort(v);
           quick_sort(v->data, 0, v->used);
         break;
     }
 }
