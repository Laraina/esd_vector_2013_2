#ifndef VECTOR_H
#define VECTOR_H
 
 #define MIN(a, b) ((a) < (b) ? (a) : (b))
 #define MAX(a, b) ((a) > (b) ? (a) : (b))
 
 /* C�digos de retorno */
 enum {
     success  = 0, /* opera��o realizada com sucesso. */
     param_e  = 1, /* erro de par�metro inv�lido.     */
     memory_e = 2, /* erro de aloca��o de mem�ria.    */
 };
 
enum {
    selection = 1,
    insertion = 2,
    merge     = 3,
    quick     = 4
};

 typedef struct {
     int *data;  /* buffer de armazenamento de dados.        */
     int  size;  /* capacidade m�xima atual.                 */
     int  used;  /* capacidade utilizada no momento.         */
     int  block; /* tamanho do bloco para redimensionamento. */
 } Vector;
 
 /**
  * Cria um novo vetor.
  *
  * param [in] block - tamanho do bloco do vetor.
  *
  * return Vector* em caso de sucesso ou NULL caso contr�rio.
  */
 Vector* vector_create(int block);
 
 /**
  * Destroi um vetor.
  *
  * param [in] v - vetor a ser desalocado.
  */
 void vector_destroy(Vector* v);
 
 /**
  * Insere um novo elemento em um vetor.
  *
  * param [in, out] v    - vetor ao qual o dado ser� inserido.
  * param [in]      pos  - posi��o da inser��o.
  * param [in]      data - valor a ser inserido.
  *
  * return Vide "C�digos de retorno".
  */
 int vector_insert(Vector* v, int pos, int data);
 
 /**
  * Remove um elemento de um vetor.
  *
  * param [in, out] v    - vetor ao qual o dado ser� removido.
  * param [in]      pos  - posi��o da remo��o.
  *
  * return Vide "C�digos de retorno".
  */
 int vector_remove(Vector* v, int pos);
 
 /**
  * Busca um elemento em um vetor.
  *
  * param [in, out] v    - vetor no qual o dado ser� procurado.
  * param [in]      data - valor a ser encontrado.
  *
  * return posi��o da primeira inst�ncia do elemento no vetor
  *        ou -1 caso o elemento n�o exista no vetor.
  */
 int vector_find(Vector* v, int data);
 
 /**
  * Contabiliza a quantidade de um determinado elemento em um vetor.
  *
  * param [in, out] v    - vetor no qual o dado ser� contabilizado.
  * param [in]      data - valor a ser contabilizado.
  *
  * return quantidade do elemento presente no vetor.
  */
 int vector_count(Vector* v, int data);
 
/**
+ * Ordena um vetor de acordo com o tipo de ordena��o.
+ *
+ * param [in, out] v       - vetor a ser ordenado.
+ * param [in]      sorting - tipo de ordena��o.
+ */
void vector_sort(Vector* v, int sorting);

 #endif

