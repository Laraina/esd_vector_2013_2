#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "vector.h"
 
 void printv(Vector* v)
 {
     int pos;
     
     printf("v(%d/%d) -> [", v->used, v->size);
     
     for (pos = 0; pos < v->used; pos++)
         printf("%d%s",
             v->data[pos],
             pos < v->used-1 ? ", " : "");
             
     printf("]\n");
 }
 
 void printe(Vector* v, unsigned int pos)
 {
     if (pos < v->used)
         printf("v[%d] -> %d\n", pos, v->data[pos]);
 }
 
 void helper(void) {
     printf("uso: PROG [argumentos]\n"
            "\nonde os argumentos s�o:"
            "\n\t i n   : Insere o n�mero 'n' na posi��o 'i' do vetor."
            "\n\t-i     : Remove o elemento de �ndice 'i' do vetor."
            "\n\t f n   : Busca o primeiro 'n' no vetor e imprime 'v[i] -> n'."
            "\n\t c n   : Imprime a quantidade de ocorr�ncias de 'n' no vetor."
            "\n\t e i   : Imprime 'v[i] -> n'."
           "\n\t print : Imprime o vetor.\n"
           "\n\t print : Imprime o vetor."
           "\n\t s n   : Classifica o vetor de acordo com n:"
           "\n\t           1 - Insertion, 2 - Selection, 3 - Merge, 4 - Quick.\n");
 }
 
 int main(int argc, char** argv)
 {
     int index, i, n;
     Vector *v = vector_create(2);
     
     if(argc < 2)
         helper();
     
     if (v == NULL)
         return memory_e;
     
     for (index = 1; index < argc; index++) {
         switch (argv[index][0]) {
             case 'e':
                 printe(v, atoi(argv[++index]));
             break;
             
             case 'f':
                 printe(v, vector_find(v, atoi(argv[++index])));
             break;
         
             case 'c':
                 printf("%d\n", vector_count(v, atoi(argv[++index])));
             break;
             
             case 'p':
                 if (!strcmp(argv[index], "print"))
                     printv(v);
             break;
         
             case '-':
                 vector_remove(v, -atoi(argv[index]));
             break;
             
            case 's':
                vector_sort(v, atoi(argv[++index]));
            break;
            
             default:
                 vector_insert(v, atoi(argv[index]), atoi(argv[index+1]));
                 index++;
             break;    
         }
     }
     
     vector_destroy(v);
     
     return success;
 }